import { Component } from '@angular/core';

import { Alumno, AlumnosData } from './entities/alumno.entity';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  alumnos: Alumno[];
  alumnoSeleccionado: Alumno;

  private _alumnosData: AlumnosData;

  constructor() {
    this._alumnosData = new AlumnosData(); // No debería ser así
    this.alumnos = this._alumnosData.GetAll();
  }

  seleccionarAlumno(alumno: Alumno) {
    this.alumnoSeleccionado = alumno;
    console.log(alumno);
  }

  Filtrar(filtrar: string) {
    this.alumnos = this._alumnosData.SearchbyNombreApellido(filtrar);
  }

}
