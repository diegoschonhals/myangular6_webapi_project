export class Alumno {
  constructor(
    public id: number,
    public nombre: string,
    public apellido: string,
    public sexoID: number,
    public activo: boolean,
    public perfilID: number
  ) {}
}

// export const alumnoData = new Alumno( 1, 'Jos�', 'Alduino', 1, true, 1 );
export class AlumnosData {

  private _alumnos: Alumno[] = [
    {id: 1, nombre: 'Juan', apellido: 'Perez', sexoID: 1, perfilID: 0, activo: true},
    {id: 2, nombre: 'Pedro', apellido: 'Garcia', sexoID: 1, perfilID: 1, activo: true},
    {id: 3, nombre: 'Ana', apellido: 'Romero', sexoID: 0, perfilID: 2, activo: true},
    {id: 4, nombre: 'Maria', apellido: 'Gutierrez', sexoID: 0, perfilID: 1, activo: true},
    {id: 5, nombre: 'Esteban', apellido: 'Smith', sexoID: 1, perfilID: 2, activo: true}
  ];

  constructor() { }

  GetAll(): Alumno[] {
    return this._alumnos;
  }
 
  Get(id: number): Alumno { 
        const  index = this._alumnos.findIndex( (a) => a.id === id); 
          if (index < 0) { 
                      return null;
                     } 
                     
            return this._alumnos[index]; 
          } 


  SearchbyNombreApellido(nombre: string) {
    return this._alumnos
                .filter(a =>
                  (a.nombre + ' ' + a.apellido).toLowerCase()
                   .indexOf(nombre.toLowerCase()) >= 0 );
  }

}
