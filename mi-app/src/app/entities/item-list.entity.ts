export class ItemList{
    constructor(
        public id: number,
        public descripcion: string
    ){}
}

export class GenericList{
    public list: ItemList[];
    public Descripcion(id: number){
        //=== mismo valor y mismo tipo de datos.
        return this.list.find(item=>item.id===id).descripcion;
    }
}
export class SexosList extends GenericList {
  constructor() {
    super();
    this.list = [
      new ItemList( 0, 'Femenino'),
      new ItemList( 1, 'Masculino'),
      new ItemList( 2, 'Otro')
    ];
  }
}

export class PerfilesList extends GenericList  {
  constructor() {
    super();
    this.list = [
      new ItemList( 0, 'Desarrollador'),
      new ItemList( 1, 'IT'),
      new ItemList( 2, 'Power User'),
      new ItemList( 3, 'DevOp'),
    ];
  }
}