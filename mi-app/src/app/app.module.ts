import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppMenuComponent } from './app-menu/app-menu.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule, MatListModule } from '@angular/material';

import { AlumnoItemComponent } from './alumno-item/alumno-item.component';
import { AlumnosListComponent } from './alumnos-list/alumnos-list.component';
import { SearchBoxComponent } from './search-box/search-box.component';
import { SignUpComponent } from './user/sign-up/sign-up.component';
import { SignInComponent } from './user/sign-in/sign-in.component';
import { UserComponent} from './user/user.component';



import {HomeComponent} from './home/home.component';
import {FormsModule} from '@angular/forms';
import { UserService } from '../app/shared/user.service';
import { HttpClientModule } from '@angular/common/http';
import { appRoutes } from './routes';
import {RouterModule} from '@angular/router';



import {ReactiveFormsModule} from "@angular/forms";
import {ListUserComponent} from "./user/list-user/list-user.component";
import { ToastrModule }  from 'ngx-toastr';
import { AddUserComponent } from './user/add-user/add-user.component';
import { EditUserComponent } from './user/edit-user/edit-user.component';

@NgModule({
  declarations: [
    AppComponent,
    AppMenuComponent,
    AlumnoItemComponent,
    AlumnosListComponent,
    SearchBoxComponent,
    SignUpComponent,
    SignInComponent,
    HomeComponent,
    UserComponent,
    ListUserComponent,
    AddUserComponent,
    EditUserComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    //UserComponent,
    ToastrModule.forRoot(),
    RouterModule.forRoot(appRoutes)
  ],
  providers: [UserService],
  bootstrap: [AppComponent]
})
export class AppModule { }
