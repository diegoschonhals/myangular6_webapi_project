import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { of } from 'rxjs';
import {User} from './user.model';

@Injectable()
export class AuthService {
  constructor(private http: HttpClient) { }
  baseUrl: string = 'http://localhost:8080/user-portal/users';

  getUsers() {
    //return this.http.get<User[]>(this.baseUrl);
    let fakeUsers = [{ FirstName: 'Dhiraj', LastName: 'Ray', Email: 'dhiraj@gmail.com'},
      { FirstName: 'Tom', LastName: 'Jac', Email: 'Tom@gmail.com'},
      { FirstName: 'Hary', LastName: 'Pan', Email: 'hary@gmail.com'},
      { FirstName: 'praks', LastName: 'pb', Email: 'praks@gmail.com'},
    ];
    return of(fakeUsers);

  }

  
}
