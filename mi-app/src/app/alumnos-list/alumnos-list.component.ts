import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Alumno } from '../entities/alumno.entity';

@Component({
  selector: 'app-alumnos-list',
  templateUrl: './alumnos-list.component.html',
  styleUrls: ['./alumnos-list.component.css']
})
export class AlumnosListComponent implements OnInit {

  @Input() alumnos: Alumno[];
  @Input() alumnoSeleccionado: Alumno; 
  @Output() Seleccionar = new EventEmitter<Alumno>(); 

  constructor() { }

  ngOnInit() {
  }

  SeleccionarAlumno(alumno: Alumno) { 
    debugger;
       this.alumnoSeleccionado = alumno;
       this.Seleccionar.emit(alumno);
   }  
}
